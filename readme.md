# Assets Management
The project is in development.
## Description
The project is built during the internship. Powered by BeeTech. Building asset management applications for businesses. Application of RFID technology.

This project using the following technologies:
- React Js for the F.E
- Express and Mongoose for the B.E
- MongoDB for the DB

## Prerequisites
- Node JS v18.5.0
- MongoDB v6.0

## Running the Project
Enable terminal console in the current project. To install packages, run command as below:
>npm install

To start server, run command as below:
>npm start

Testing site: 
>http://localhost:3000
