/* eslint-disable quotes */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
// eslint-disable-next-line no-unused-vars
import request from 'supertest';
import jwt from 'jsonwebtoken';
import mongoose from 'mongoose';

import { app } from '../src/app.js';
import { User } from '../src/models/user.js';
import { privatekey } from '../src/midlerware/auth.js';

const userOneId = new mongoose.Types.ObjectId();
const userOne = {
    _id: userOneId,
    name: 'Tran Ngoc Thinh',
    email: 'ngocthinh@gmail.com',
    password: 'Abc123123',
    age: 23,
    tokens: [{
        token: jwt.sign({ _id: userOneId }, privatekey, { algorithm: 'RS256' })
    }]
};
// jest.useRealTimers();
beforeEach(async () => {
    await User.deleteMany();
    await new User(userOne).save();
});

test('Should signup a new user', async () => {
    await request(app).post('/users').send({
        name: 'Nguyen Ngoc Duy',
        email: 'duynguyen960416@gmail.com',
        password: 'Abc123123',
        age: 23
    }).expect(201);
});

test('Should login existing user', async () => {
    await request(app).post('/users/login').send({
        email: userOne.email,
        password: userOne.password,
    }).expect(200);
});

test('Should not login existing user with wrong password', async () => {
    await request(app).post('/users/login').send({
        email: userOne.email,
        password: '123123123',
    }).expect(400);
});

test('Should not login existing user', async () => {
    await request(app).post('/users/login').send({
        email: 'ngocthinh@gmail.com',
        password: 'Abc1231',
    }).expect(400);
});

test('Should get profile for user', async () => {
    await request(app)
        .get('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200);
});
test('Should not get profile for user', async () => {
    await request(app)
        .get('users/me')
        .set('Authorization', `Bearer 213123123213`)
        .send()
        .expect(404);
});

