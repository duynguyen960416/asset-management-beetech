/* eslint-disable no-extra-semi */
/* eslint-disable no-unused-vars */
import express from 'express';
import { connect } from './db/db.js';
import responseTime from 'response-time';
import amqplib from 'amqplib';

import { router as productRouter } from './routers/product.js';
import { router as userRouter } from './routers/user.js';
import path from 'path';
import { fileURLToPath } from 'url';

const app = express();
var channel, conn;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const publicDirectoryPath = path.join(__dirname, '../src/public');

app.use(responseTime());
app.use(express.json());
app.use(productRouter);
app.use(userRouter);
app.use(express.static(publicDirectoryPath));
//Connect DB
connect();
async function connectRabbitMQ() {
    conn = await amqplib.connect('amqp://localhost:5672', (error) => {
        console.log(error);
    });
    channel = await conn.createChannel();
    await channel.assertQueue('GET_PRODUCT');
    console.log('Connect RabbitMQ Successfully');
};
connectRabbitMQ();
export { app, channel };