import { errorFunction } from '../errorFunction.js';

export const paginatedResults = (model) => {
    return async (req, res, next) => {
        const limit = parseInt(req.query.limit);
        const page = parseInt(req.query.page);
        const startIndex = (page - 1) * limit;
        const endIndex = page * limit;
        const total = await model.countDocuments({}).exec();
        console.log('Limit: ' + limit);
        console.log('Page: ' + page);
        console.log('Start Index: ' + startIndex);
        console.log('End Index: ' + endIndex);
        console.log('Count Documents: ' + total);
        const results = {};

        if (endIndex < total) {
            results.next = {
                page: page + 1,
                limit: limit
            };
        }
        if (startIndex > 0) {
            results.previouss = {
                page: page - 1,
                limit: limit
            };
        }


        try {
            results.result = await model.find({}).limit(limit).skip(startIndex).exec();
            res.paginatedResults = results;
            next();
        } catch (error) {
            // res.status(400).send(error);
            res.json(errorFunction(400, 'Can not find Document!', error));
        }
    };
};

