import Joi from 'joi';
import { errorFunction } from '../errorFunction.js';

export const validateUser = Joi.object({
    name: Joi.string().required().min(3),
    email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
    password: Joi.string().required().min(6).pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    age: Joi.number().required()

});
export const userValidation = async (req, res, next) => {
    const payload = {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        age: req.body.age
    };
    const { error } = validateUser.validate(payload);
    if (error) {
        console.log('Error! ' + error);
        // eslint-disable-next-line quotes
        return res.json(errorFunction(406, `Error in User Data : `, error.message));
    } else {
        next();
    }
};