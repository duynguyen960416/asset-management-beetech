import Joi from 'joi';
import { errorFunction } from '../errorFunction.js';

export const validateProduct = Joi.object({
    name: Joi.string().min(3).required(),
    brand: Joi.string().min(3).required(),
    description: Joi.string(),
    quantity: Joi.number().integer().required(),
    category: Joi.string().required(),
    approvalDate: Joi.date().required().less('now')
});

export const productValidation = async (req, res, next) => {
    const payload = {
        name: req.body.name,
        brand: req.body.brand,
        description: req.body.description,
        quantity: req.body.quantity,
        category: req.body.category,
        approvalDate: req.body.approvalDate,
    };

    const { error } = validateProduct.validate(payload);
    if (error) {
        console.log('Error! ' + error);
        // res.status(406);
        // eslint-disable-next-line quotes
        return res.json(errorFunction(406, 'Error in Product Data!', error.message));

    } else {
        next();
    }
};

