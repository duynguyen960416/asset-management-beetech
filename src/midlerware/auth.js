import jwt from 'jsonwebtoken';
import { User } from '../models/user.js';

import { errorFunction } from '../errorFunction.js';

import fs from 'fs';
export const privatekey = fs.readFileSync('./private.key', 'utf8');
export const publickey = fs.readFileSync('./public.key', 'utf8');

export const auth = async (req, res, next) => {
    try {
        const token = req.header('Authorization').replace('Bearer ', '');
        const decodedToken = jwt.verify(token, publickey, { algorithm: 'RS256' });
        const user = await User.findOne({ _id: decodedToken._id, 'tokens.token': token });
        console.log(user);
        if (!user) {
            throw new Error();
        }
        req.token = token;
        req.user = user;
        next();
    } catch (error) {
        console.log(error);
        res.status(401);
        res.json(errorFunction(401, 'Invalid token!', error));
    }
};

