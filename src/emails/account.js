import sgMail from '@sendgrid/mail';


const sendgridApiKey = process.env.SENDGRID_API_KEY;

sgMail.setApiKey(sendgridApiKey);

export const sendforgotpasswdEmail = (from, receiver, subject, content) => {
    try {
        const message = {
            from: from,
            to: receiver,
            subject: subject,
            html: content
        };
        return sgMail.send(message);
    } catch (error) {
        return new Error(error);
    }
};

