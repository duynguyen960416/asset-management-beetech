/* eslint-disable no-unused-vars */

import { app } from './app.js';

import http from 'http';
import { Server } from 'socket.io';

const port = process.env.PORT;

const server = http.createServer(app);
const io = new Server(server);

// app.get('/health', (req, res) => {
//     const data = {
//         uptime: process.uptime(),
//         message: 'Ok',
//         date: new Date()
//     };
//     res.status(200).send(data);
// });

// eslint-disable-next-line no-unused-vars
// let count = 0;
// io.on('connection', (socket) => {
//     console.log('New connection!');
//     setInterval(() => {
//         socket.emit('countUpdated', count++);
//     }, 1000);
// });


app.listen(port, () => {
    console.log('Server is up on port ' + port);
});
