export const errorFunction = (status, msg, data) => {
    if (status <= 200 || status >= 226) {
        return { status: status, message: msg, error: data };
    } else {
        return { status: status, message: msg, data: data };
    }
};