/* eslint-disable no-unused-vars */
import redis from 'redis';


const redisPort = process.env.REDIS_PORT || 6379;
const client = redis.createClient(redisPort);


client.on('connection', (err) => {
    console.log('Redis connection Establised!');
});
client.on('error', (err) => console.log('Redis Client Error', err));

