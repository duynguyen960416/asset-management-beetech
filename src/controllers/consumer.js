/* eslint-disable no-extra-semi */
import amqplib from 'amqplib';

var channel, conn;

async function connectRabbitMQ() {
    conn = await amqplib.connect('amqp://localhost:5672', (error) => {
        console.log(error);
    });
    channel = await conn.createChannel();
    await channel.assertQueue('GET_PRODUCT');
    console.log('Connect AMQP Successfully');
};
connectRabbitMQ().then(() => {
    channel.consume('GET_PRODUCT', (data) => {
        console.log('Get product', JSON.parse(data.content));
    });
});