import amqplib from 'amqplib';

(async () => {
    const queue = 'tasks';
    const conn = await amqplib.connect('amqp://localhost');

    
    

    // Sender
    const ch2 = await conn.createChannel();

    setInterval(() => {
        ch2.sendToQueue(queue, Buffer.from('something to do'));
    }, 1000);
})();