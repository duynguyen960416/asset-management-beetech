import express from 'express';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
// import multer from 'multer';
import { uploadAvatar, uploadImage } from '../midlerware/files-controller.js';


import { User } from '../models/user.js';
import { auth } from '../midlerware/auth.js';
import { sendforgotpasswdEmail } from '../emails/account.js';
import { paginatedResults } from '../midlerware/paginated.js';
import { userValidation } from '../midlerware/user.js';
import { errorFunction } from '../errorFunction.js';


const router = new express.Router();



router.post('/users', userValidation, async (req, res) => {
    const user = new User(req.body);
    try {
        await user.save();
        const token = await user.generateAuthToken();
        res.status(201).send({ user, token });
    } catch (e) {
        res.status(400);
        res.json(errorFunction(400, 'Bad Request!'));
    }
});
router.get('/users', auth, paginatedResults(User), async (req, res) => {
    try {
        const user = await User.find({});
        res.send(user);
    } catch (e) {
        console.log(e);
        res.status(500);
        res.json(errorFunction(500, 'Internal Server Error!'));
    }

});

router.post('/users/login', async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken();
        res.send({ user, token });
    } catch (e) {
        console.log('Error: ' + e);
        res.status(400);
        res.json(errorFunction(400, 'Bad Request !'));
    }
});

router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token;
        });
        await req.user.save();
        res.send();
    } catch (error) {
        console.log(error);
        res.status(500);
        res.json(errorFunction(500, 'Internal Server Error'));
    }
});

router.get('/users/me', auth, async (req, res) => {
    res.send(req.user);
});

router.patch('/users/me', auth, async (req, res) => {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['name', 'password', 'age'];
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update));
    if (!isValidOperation) {
        return res.json(errorFunction(400, 'Invalid updates'));
    }
    try {
        updates.forEach((update) => req.user[update] = req.body[update]);
        await req.user.save();
        res.send(req.user);
    } catch (e) {
        res.status(400);
        res.json(errorFunction(400, 'Invalid updates'));
    }
});

router.delete('/users/me', auth, async (req, res) => {
    try {
        req.user.remove();
        res.send(req.user);
    } catch (e) {
        res.status(500);
        res.json(errorFunction(500, 'Internal Server Error!'));
    }

});

router.put('/forgot-password', async (req, res) => {
    const email = req.body.email;
    try {
        const user = await User.findOne({ email }).lean();
        if (!user) {
            return res.status(400).json({ error: 'User with this email does not exist!' });
        } else {
            console.log('Email exists!');
            const token = jwt.sign({ _id: user._id }, process.env.JWT_FORGOT_PASSWORD_KEY);
            await sendforgotpasswdEmail(
                process.env.SENDGRID_FROM_EMAIL,
                email,
                'Account forgot password link',
                `<h2>Please click on given link to reset your password account</h2>
                <div>http://localhost:3000/reset_password/${token}</div>`
            );
            return res.status(200).send({ message: 'Email has been sent!' });
        }
    } catch (error) {
        console.log(error);
        res.status(400);
        return res.json(errorFunction(400, 'Bad Request!'));
    }
});
router.post('/reset-password/:token', async (req, res) => {
    try {
        const password = req.body.password;
        const token = req.params.token;
        console.log(token);
        const decoded = jwt.verify(token, process.env.JWT_FORGOT_PASSWORD_KEY);
        const hashPassword = await bcrypt.hash(password, 8);
        const updateUser = await User.findByIdAndUpdate({ _id: decoded._id }, { password: hashPassword });
        return res.status(200).send({ token, user: updateUser });
    } catch (error) {
        console.log(error);
        res.status(400);
        return res.json(errorFunction(400, 'Bad Request!'));
    }
});

router.post('/users/me/avatar', auth, uploadAvatar.single('avatar'), async (req, res) => {
    req.user.avatar = req.file.buffer;
    await req.user.save();
    res.send();
}, (error, req, res) => {
    res.status(400);
    res.json(errorFunction(400, 'Bad Request!'));
});

router.post('/users/me/image', auth, uploadImage.single('image'), async (req, res, next) => {
    const file = req.file;
    if (!file) {
        const error = new Error('Please upload a file');
        error.httpStatusCode = 400;
        return next(error);
    }
    return res.send({
        filename: file.filename,
        url: 'http://localhost:3000/images/' + file.filename
    });

});
// , (error, req, res) => {
//     res.status(400).send({ message: error.message });
// });

router.delete('/users/me/avatar', auth, async (req, res) => {
    req.user.avatar = undefined;
    await req.user.save();
    res.send();
});

export { router };