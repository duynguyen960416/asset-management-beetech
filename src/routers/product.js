import express from 'express';
import { paginatedResults } from '../midlerware/paginated.js';
import { Product } from '../models/product.js';
import { productValidation } from '../midlerware/product.js';
import { errorFunction } from '../errorFunction.js';
import { channel } from '../app.js';

export const router = new express.Router;
router.post('/products', productValidation, async (req, res) => {
    try {
        const existingProduct = await Product.findOne({ name: req.body.name }).lean(true);
        if (existingProduct) {
            return res.status(403).send('Product Already Exists');
        } else {
            const product = await Product.create({
                name: req.body.name,
                brand: req.body.brand,
                description: req.body.description,
                quantity: req.body.quantity,
                category: req.body.category,
                approvalDate: req.body.approvalDate,
            });
            if (product) {
                res.status(201);
                return res.json(errorFunction(201, 'Created', product));
            } else {
                res.status(403);
                return res.json(errorFunction(403, 'Forbiden!'));
            }
        }
    } catch (error) {
        console.log(error);
        res.status(400).send();
        res.json(errorFunction(400, 'Bad Request!', error));
    }
});
router.get('/products', paginatedResults(Product), (req, res) => {
    res.json(res.paginatedResults);
});
router.get('/product/:id', async (req, res) => {
    const _id = req.params.id;
    try {
        const product = await Product.findById(_id);
        if (!product) {
            res.status(404);
            res.json(errorFunction(404, 'Product Not Found!'));
        }
        channel.sendToQueue('GET_PRODUCT', Buffer.from(
            JSON.stringify({
                product
            })
        ));
        return res.send(product);

    } catch (e) {
        res.status(500);
        res.json(errorFunction(500, 'Internal Server Error'));
    }
});
router.patch('/product/:id', async (req, res) => {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['name', 'description', 'quantity', 'category', 'approvalDate'];
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update));
    if (!isValidOperation) {
        res.status(400);
        return res.json(errorFunction(400, 'Invalid Updates'));
    }
    try {
        const product = await Product.findByIdAndUpdate(req.params.id, req.body, { new: true, runValidator: true });
        if (!product) {
            res.status(404);
            return res.json(errorFunction(404, 'Product Not Found!'));
        }
        res.send(product);
    } catch (e) {
        res.status(400);
        res.json(errorFunction(400, 'Bad Request!'));
    }
});
router.delete('/product/:id', async (req, res) => {
    try {
        const product = await Product.findByIdAndDelete(req.params.id);
        if (!product) {
            res.status(404);
            return res.json(errorFunction(404, 'Product Not Found!'));
        }
        res.send(product);
    } catch (e) {
        res.status(500);
        res.json(errorFunction(500, 'Internal Server Error'));
    }
});
