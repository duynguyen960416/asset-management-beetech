import mongoose from 'mongoose';


const ProductSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true
    },
    brand: {
        type: String,
        required: true,
    },
    category: {
        type: String,
        required: true,
    },
    quantity: {
        type: Number,
        required: true,
    },
    approvalDate: {
        type: Date,
        required: true,
    }
}, {
    timestamp: true
});

export const Product = mongoose.model('Product', ProductSchema);
