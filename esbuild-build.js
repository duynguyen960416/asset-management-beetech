/* eslint-disable no-undef */
import esbuild from 'esbuild';

esbuild.buildSync({
    entryPoints: ['src/index.js'],
    bundle: true,
    minify: true,
    format: 'esm',
    outfile: 'src/public/dist/app.js',
    platform: 'node',
});